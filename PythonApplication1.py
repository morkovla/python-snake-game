import random
import pyglet
import numpy
import numpy as np
from pyglet.gl import *
from pyglet.window import key

map = pyglet.graphics.Batch()
worm = pyglet.graphics.Batch()

point_image = pyglet.image.load('Point.png')
Point = pyglet.sprite.Sprite(img=point_image)

wall_image = pyglet.image.load('Wall.png')
wall_fill = wall_image.get_region(x=17, y=17, width=16, height=16)
wall_ulc = pyglet.sprite.Sprite(img=wall_image.get_region(x=0, y=34, width=16, height=16), x=0, y=16 * 48, batch=map)
wall_urc = pyglet.sprite.Sprite(img=wall_image.get_region(x=34, y=34, width=16, height=16), x=16 * 48, y=16 * 48,
                                batch=map)
wall_dlc = pyglet.sprite.Sprite(img=wall_image.get_region(x=0, y=0, width=16, height=16), x=0, y=0, batch=map)
wall_drc = pyglet.sprite.Sprite(img=wall_image.get_region(x=34, y=0, width=16, height=16), x=16 * 48, y=0, batch=map)

wall_c1 = wall_image.get_region(x=51, y=0, width=16, height=16)
wall_c2 = wall_image.get_region(x=51, y=17, width=16, height=16)
wall_c3 = wall_image.get_region(x=51, y=34, width=16, height=16)

wall_lw = wall_image.get_region(x=0, y=17, width=16, height=16)
wall_rw = wall_image.get_region(x=34, y=17, width=16, height=16)
wall_uw = wall_image.get_region(x=17, y=34, width=16, height=16)
wall_dw = wall_image.get_region(x=17, y=0, width=16, height=16)
wall_ulc.scale = 3
wall_urc.scale = 3
wall_dlc.scale = 3
wall_drc.scale = 3

direction1 = 0
direction2 = 0
point = False

wall = pyglet.graphics.Batch()
file_map = np.zeros((17, 17), dtype=bool)
map_sprites = []


class Snake:
    def __init__(self, x, y, direct):
        self.x = x
        self.y = y
        self.direction = direct
        self.score = 0
        self.alive = True
        self.move = True
        self.body = np.array([[y, x]], dtype=int)

    def update(self):
        global point, Point, file_map
        if self.direction == 0:
            self.body = np.insert(self.body, 0, [self.body[0, 0] + 1, self.body[0, 1]], axis=0)
        elif self.direction == 1:
            self.body = np.insert(self.body, 0, [self.body[0, 0], self.body[0, 1] + 1], axis=0)
        elif self.direction == 2:
            self.body = np.insert(self.body, 0, [self.body[0, 0] - 1, self.body[0, 1]], axis=0)
        elif self.direction == 3:
            self.body = np.insert(self.body, 0, [self.body[0, 0], self.body[0, 1] - 1], axis=0)

        file_map[self.body[int(self.body.size / 2 - 1), 0], self.body[int(self.body.size / 2 - 1), 1]] = False

        if file_map[self.body[0, 0], self.body[0, 1]]:
            self.alive = False
            self.body = np.delete(self.body, 0, 0)
            file_map[self.body[int(self.body.size / 2 - 1), 0], self.body[int(self.body.size / 2 - 1), 1]] = True
            return
        else:
            file_map[self.body[0, 0], self.body[0, 1]] = True

        if self.body[0, 0] == int(Point.y / 48) and self.body[0, 1] == int(Point.x / 48):
            point = False
        else:
            self.body = np.delete(self.body, int(self.body.size / 2 - 1), 0)

    def draw(self):
        snake_batch = []
        for [y, x] in self.body:
            if [y, x] == [self.body[0, 0], self.body[0, 1]] or not self.alive:
                img = pyglet.sprite.Sprite(img=wall_c3, x=48 * x, y=48 * y, batch=worm)
            else:
                img = pyglet.sprite.Sprite(img=wall_c2, x=48 * x, y=48 * y, batch=worm)
            img.scale = 3
            snake_batch.append(img)
        worm.draw()

    def BFS(self):
        n = 1

        queue = [[int(Point.y / 48), int(Point.x / 48)]]
        while True:
            explored = []
            while queue:
                path = queue.pop(0)

                if path not in explored and not (file_map[path[0], path[1]]):

                    queue.append([path[0] + 1, path[1]])
                    queue.append([path[0] - 1, path[1]])
                    queue.append([path[0], path[1] + 1])
                    queue.append([path[0], path[1] - 1])

                    explored.append(path)
                    if [self.body[0, 0], self.body[0, 1]] in queue:
                        return path

            ty = self.body[len(self.body) - n, 0]
            tx = self.body[len(self.body) - n, 1]
            queue.append([ty + 1, tx])
            queue.append([ty - 1, tx])
            queue.append([ty, tx + 1])
            queue.append([ty, tx - 1])
            n += 1
            if n >= len(self.body): return


snake1 = Snake(6, 6, 0)
snakes = [snake1]
#########################################################

print("Choose gameplay mode:")
print("    1: One player")
print("    2: Two players")

num = int(input())
if num == 1:
    print("Choose control mode:")
    print("    1: User")
    print("    2: Computer")
    firstcontrol = int(input())
elif num == 2:
    snake2 = Snake(10, 10, 0)
    snakes.append(snake2)
    print("Choose first player control mode:")
    print("    1: User")
    print("    2: Computer")
    firstcontrol = int(input())
    print("Choose second player control mode:")
    print("    1: User")
    print("    2: Computer")
    secondcontrol = int(input())

print("Choose map type:")
print("    1: Simple")
print("    2: Hard")
num3 = int(input())

window = pyglet.window.Window(816, 816)

glEnable(GL_BLEND)
glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)


#########################################################

def loadMap():
    if num3 == 1:
        file = open('Map1.txt', 'r')
    if num3 == 2:
        file = open('Map2.txt', 'r')
    y = 0
    for line in file:
        x = 0
        for c in line:
            if c is '1':
                file_map[y, x] = True
            x += 1
        y += 1

    for x in (wall_ulc, wall_urc, wall_dlc, wall_drc):
        map_sprites.append(x)

    for i in range(15):
        img = pyglet.sprite.Sprite(img=wall_uw, x=48 * (i + 1), y=0, batch=map)
        img.scale = 3
        map_sprites.append(img)
        img = pyglet.sprite.Sprite(img=wall_uw, x=48 * (i + 1), y=48 * 16, batch=map)
        img.scale = 3
        map_sprites.append(img)
        img = pyglet.sprite.Sprite(img=wall_lw, x=0, y=48 * (i + 1), batch=map)
        img.scale = 3
        map_sprites.append(img)
        img = pyglet.sprite.Sprite(img=wall_rw, x=48 * 16, y=48 * (i + 1), batch=map)
        img.scale = 3
        map_sprites.append(img)

    for y in range(1, 16):
        for x in range(1, 16):
            if file_map[y, x]:
                img = pyglet.sprite.Sprite(img=wall_c1, x=48 * x, y=48 * y, batch=map)
            else:
                img = pyglet.sprite.Sprite(img=wall_fill, x=48 * x, y=48 * y, batch=map)
            img.scale = 3
            map_sprites.append(img)


def update(dt):
    global snakes, point, Point

    while not point:
        a = random.randint(1, 16)
        b = random.randint(1, 16)
        if not (file_map[a, b]):
            point = True
            Point = pyglet.sprite.Sprite(img=point_image, y=a * 48, x=b * 48)
            Point.scale = 3

    if snakes[0].alive:
        if firstcontrol == 2:
            step = snakes[0].BFS()
            if step:
                if step[0] > snakes[0].body[0, 0]: snakes[0].direction = 0
                if step[0] < snakes[0].body[0, 0]: snakes[0].direction = 2
                if step[1] > snakes[0].body[0, 1]: snakes[0].direction = 1
                if step[1] < snakes[0].body[0, 1]: snakes[0].direction = 3
        snakes[0].update()
        snakes[0].move = True

    if len(snakes) == 2:
        if snakes[1].alive:
            if secondcontrol == 2:
                step = snakes[1].BFS()
                if step:
                    if step[0] > snakes[1].body[0, 0]: snakes[1].direction = 0
                    if step[0] < snakes[1].body[0, 0]: snakes[1].direction = 2
                    if step[1] > snakes[1].body[0, 1]: snakes[1].direction = 1
                    if step[1] < snakes[1].body[0, 1]: snakes[1].direction = 3
            snakes[1].update()
            snakes[1].move = True


@window.event
def on_draw():
    global snakes
    window.clear()
    map.draw()
    Point.draw()
    for x in snakes:
        x.draw()


@window.event
def on_key_press(key_code, modifier):
    if key_code == key.Q: window.close()
    global snakes
    if firstcontrol == 1:
        if snakes[0].move:
            if key_code == key.UP and snake1.direction != 2:
                snake1.direction = 0
            elif key_code == key.LEFT and snake1.direction != 1:
                snake1.direction = 3
            elif key_code == key.RIGHT and snake1.direction != 3:
                snake1.direction = 1
            elif key_code == key.DOWN and snake1.direction != 0:
                snake1.direction = 2
        if key_code == key.UP or key_code == key.LEFT or key_code == key.RIGHT or key_code == key.DOWN:
            snakes[0].move = False

    if len(snakes) == 2 and secondcontrol == 1:
        if snakes[1].move:
            if key_code == key.W and snake2.direction != 2:
                snake2.direction = 0
            elif key_code == key.A and snake2.direction != 1:
                snake2.direction = 3
            elif key_code == key.D and snake2.direction != 3:
                snake2.direction = 1
            elif key_code == key.S and snake2.direction != 0:
                snake2.direction = 2
        if key_code == key.W or key_code == key.A or key_code == key.D or key_code == key.S: snakes[1].move = False


if __name__ == "__main__":
    loadMap()
    if firstcontrol == 1:
        pyglet.clock.schedule_interval(update, 1 / 5)
    if firstcontrol == 2:
        pyglet.clock.schedule_interval(update, 1 / 10)
    pyglet.app.run()
