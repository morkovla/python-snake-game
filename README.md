## **Abyste mohli začít hrát je nutné:**
1. Splnit požadavky popsané v requirements.txt
2. Spustit PythonApplication1.py
3. Vybrat herní režim
4. Hrát! 
    Pro prvního hráče použijte **šipky** na klávesnici
	Pokud budete hrát s někým spolu, můžete ještě použít **WASD**
	Cílem hry je pěstovat co největšího hada
5. Můžete ukončit hru pomocí klávesy **Q**
