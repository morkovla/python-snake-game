import PythonApplication1
import pyglet
import numpy
import numpy as np
from pyglet.gl import *
from pyglet.window import key

if __name__ == "__main__":
    PythonApplication1.Point.x = 6 * 48
    PythonApplication1.Point.y = 8 * 48

    PythonApplication1.file_map[8, 8] = True
    PythonApplication1.point = True

    snake1 = PythonApplication1.Snake(6, 6, 0)

    assert snake1.alive is True
    assert len(snake1.body) == 1
    assert snake1.x == 6
    assert snake1.y == 6
    assert snake1.direction == 0
    print("OK, snake on the map")

    snake1.update()
    assert snake1.alive is True
    assert len(snake1.body) == 1

    assert snake1.body[0, 0] == 7
    assert snake1.body[0, 1] == 6
    print("OK, move up")

    snake1.update()
    assert PythonApplication1.point is False

    assert snake1.alive is True
    assert len(snake1.body) == 2

    assert snake1.body[0, 0] == 8
    assert snake1.body[0, 1] == 6
    assert snake1.body[1, 0] == 7
    assert snake1.body[1, 1] == 6
    print("OK, take a bonus")

    snake1.direction = 1

    snake1.update()

    assert snake1.alive is True
    assert len(snake1.body) == 2

    assert snake1.body[0, 0] == 8
    assert snake1.body[0, 1] == 7
    assert snake1.body[1, 0] == 8
    assert snake1.body[1, 1] == 6
    print("OK, turn works")

    snake1.update()

    assert snake1.alive is False
    assert len(snake1.body) == 2

    assert snake1.body[0, 0] == 8
    assert snake1.body[0, 1] == 7
    assert snake1.body[1, 0] == 8
    assert snake1.body[1, 1] == 6
    print("OK, the snake is dead")

    num = input()
